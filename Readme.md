### High Performance Computing
#### Autumn, 2019

Codes, slides, exercises and solutions are (or will be) included in the course repo. Syncing your
fork and then updating your clone (using *git pull*) will give you your own copy
of all of these files.
